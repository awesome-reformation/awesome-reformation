Videos
======

- [If God is sovereign, How can man be free? - RC Sproul](https://www.youtube.com/watch?v=iokVMSaLhvU)
- [Christopher Hitchens vs. Douglas Wilson Debate at Westminster](https://www.youtube.com/watch?v=g6UU9C-WmvM)
- [Bill Nye debates Ken Ham](https://www.youtube.com/watch?v=z6kgvhG3AkI)