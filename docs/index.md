Awesome Reformation
===================

[awesome-reformation.com](https://awesome-reformation.com) is a curated list of resources for Christian theology, especially of the reformed tradition.

Resource categories
-------------------

- Articles
- [Podcasts](podcasts.md)
- Books
- [Videos](videos.md)
- Audios
- Blogs
- Churches

How to Contribute
-----------------

### Use GitLab

Create an account on [GitLab](https://gitlab.com), go to gitlab.com/awesome-reformation/awesome-reformation, and create an issue or fork the repository, make your local changes and create a merge request.

### Other Ways

Simply send an email to [admin@awesome-reformation.com](mailto:admin@awesome-reformation.com) or if you use twitter, send a tweet to [@awesome-reformation](https://twitter.com/awesome-reformation)

Contact
-------

Contact us at [admin@awesome-reformation.com](mailto:admin@awesome-reformation.com) or [@awesome-reformation](https://twitter.com/awesome-reformation)

Footer
------

BUILDDATE @ COMMITHASH
