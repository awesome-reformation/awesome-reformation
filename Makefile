login:
	docker login registry.gitlab.com

build-image: login
	docker build -t registry.gitlab.com/awesome-reformation/awesome-reformation .

push-image: build-image
	docker push registry.gitlab.com/awesome-reformation/awesome-reformation

clean:
	rm -fr site

generate: clean
	mkdocs build
	cat site/index.html | sed "s/BUILDDATE/$$(date)/" > site/index.html
	cat site/index.html | sed "s/COMMITHASH/$$(git rev-parse HEAD)/" > site/index.html

deploy: generate
	aws s3 sync --sse --delete site/ s3://awesome-reformation.com
	aws configure set preview.cloudfront true
	aws cloudfront create-invalidation --distribution E3CCDT7BRZVTZT --path '/*'
