FROM python:2.7-alpine
COPY ["docs/", "/docs"]
COPY ["mkdocs.yml", "/mkdocs.yml"]
COPY ["requirements.txt", "/requirements.txt"]
RUN apk update && apk add make git && pip install -r requirements.txt
